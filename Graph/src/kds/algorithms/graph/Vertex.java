package kds.algorithms.graph;

class Vertex implements Comparable<Vertex> {
	private final int id;
	private int distance;
	
	public Vertex(int id) {
		this.id = id;
		this.distance = Integer.MAX_VALUE;
	}
	
	@Override
	public int compareTo(Vertex v) {
		return (distance < v.distance) ? -1 : (distance > v.distance) ? 1 : 0;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Vertex && ((Vertex) o).id == id;
	}
	
	public int getDistance() {
		return distance;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	public int id() {
		return id;
	}

	@Override
	public String toString() {
		return "Vertex[id=" + id +", distance=" + distance + "]";
	}

	public void updateDistanceIfShorter(int newDistance) {
		if (newDistance < distance) {
			distance = newDistance;
		}
	}
}