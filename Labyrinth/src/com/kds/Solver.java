package com.kds;

public class Solver {

	public static void main(String[] args) {
		Maze maze = new Maze(32, 6);
		Position exit = new Position(31, 5);
		maze.removeWall(exit, Step.EAST);
		Builder builder = new Builder();
		builder.build(maze, exit);
		maze.clearCells();
		Solver solver = new Solver();
		if (solver.solve(maze, new Position(0, 0))) {
			System.out.println(maze);
		} else {
			System.out.println("no way out");
		}
	}
	
	public boolean solve(Maze maze, Position p) {
		maze.visit(p);
		for (Step s : Step.all()) {
			if (maze.isExit(p, s)) {
				return true;
			}
			if (!maze.isBlocked(p, s)) {
				Position p2 = p.move(s);
				if (maze.isEmpty(p2)) {
					if (solve(maze, p2)) {
						maze.markAsExit(p2);
						return true;
					}
				}
			}
		}
		return false;
	}
}
