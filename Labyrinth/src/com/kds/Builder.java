package com.kds;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Builder {

	public static void main(String[] args) {
		Maze maze = new Maze(5, 4);
		Position exit = new Position(0, 0);
		maze.removeWall(exit, Step.NORTH);
		Builder builder = new Builder();
		builder.build(maze, exit);
		System.out.println(maze);
	}
	
	private Random rand = new Random();
	
	public void build(Maze maze, Position p) {
		maze.visit(p);
		List<Step> steps = new LinkedList<Step>(Step.all());
		for (int n = steps.size(); n > 0; n--) {
			int i = rand.nextInt(n);
			Step s = steps.remove(i);
			Position p2 = p.move(s);
			if (maze.isInside(p2) && maze.isEmpty(p2)) {
				maze.removeWall(p, s);
				build(maze, p2);
			}
		}
	}
}
