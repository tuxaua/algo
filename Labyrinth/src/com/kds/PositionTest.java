package com.kds;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class PositionTest {

	@Test
	public void testMove() {
		Position p1 = new Position(4, 7);
		for (Step s : Step.all()) {
			Position p2 = p1.move(s);
			assertEquals(p1.x() + s.dx(), p2.x());
			assertEquals(p1.y() + s.dy(), p2.y());
			p1 = p2;
		}
	}

}
