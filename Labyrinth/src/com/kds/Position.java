package com.kds;

class Position {
	private final int x;
	private final int y;
	
	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public Position move(Step s) {
		return new Position(x + s.dx(), y + s.dy());
	}
	
	public int x() {
		return x;
	}
	
	public int y() {
		return y;
	}
}
