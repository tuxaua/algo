package com.kds;

import java.util.Arrays;

class Maze {
	private enum State {
		EMPTY, VISITED, WAYOUT
	};

	private static void fillWalls(boolean[][] wall, boolean b) {
		for (int i = 0; i < wall.length; i++) {
			Arrays.fill(wall[i], b);
		}
	}

	private final State[][] cell;
	private final boolean[][] northWall;
	private final boolean[][] westWall;

	public Maze(int sizeX, int sizeY) {
		cell = new State[sizeX][sizeY];
		clearCells();
		northWall = new boolean[sizeX][sizeY + 1];
		fillWalls(northWall, true);
		westWall = new boolean[sizeX + 1][sizeY];
		fillWalls(westWall, true);
	}

	public void clearCells() {
		for (int x = 0; x < cell.length; x++) {
			Arrays.fill(cell[x], State.EMPTY);
		}
	}

	public boolean isBlocked(Position p, Step s) {
		int x = p.x();
		int y = p.y();

		if (s.dx() < 0) {
			return westWall[x][y];
		} else if (s.dx() > 0) {
			return westWall[x + 1][y];
		}
		if (s.dy() < 0) {
			return northWall[x][y];
		} else if (s.dy() > 0) {
			return northWall[x][y + 1];
		}
		return false;
	}

	public boolean isEmpty(Position p) {
		if (!isInside(p)) {
			throw new IllegalArgumentException("position is outside maze");
		}
		return cell[p.x()][p.y()] == State.EMPTY;
	}

	public boolean isExit(Position p, Step s) {
		int x = p.x();
		int y = p.y();
		int x2 = x + s.dx();
		int y2 = y + s.dy();
		return (x2 < 0 && !westWall[x][y])
				|| (x2 >= cell.length && !westWall[x + 1][y])
				|| (y2 < 0 && !northWall[x][y])
				|| (y2 >= cell[x].length && !northWall[x][y + 1]);
	}

	public boolean isInside(Position p) {
		int x = p.x();
		int y = p.y();
		return x >= 0 && x < cell.length && y >= 0 && y < cell[x].length;
	}

	public void markAsExit(Position p) {
		cell[p.x()][p.y()] = State.WAYOUT;
	}

	public void removeWall(Position p, Step s) {
		int x = p.x();
		int y = p.y();

		if (s.dx() < 0) {
			westWall[x][y] = false;
		} else if (s.dx() > 0) {
			westWall[x + 1][y] = false;
		}
		if (s.dy() < 0) {
			northWall[x][y] = false;
		} else if (s.dy() > 0) {
			northWall[x][y + 1] = false;
		}
	}

	@Override
	public String toString() {
		int sizeX = cell.length;
		int sizeY = cell[0].length;
		StringBuilder b = new StringBuilder();

		for (int y = 0; y < sizeY; y++) {
			for (int x = 0; x < sizeX; x++) {
				b.append(northWall[x][y] ? "+-" : "+ ");
			}
			b.append("+\n");
			for (int x = 0; x < sizeX; x++) {
				b.append(westWall[x][y] ? '|' : ' ');
				b.append(cell[x][y] == State.WAYOUT ? '*'
						: cell[x][y] == State.VISITED ? '.' : ' ');
			}
			if (westWall[sizeX][y]) {
				b.append('|');
			}
			b.append('\n');
		}
		for (int x = 0; x < sizeX; x++) {
			b.append(northWall[x][sizeY] ? "+-" : "+ ");
		}
		b.append("+\n");
		return b.toString();
	}

	public void visit(Position p) {
		cell[p.x()][p.y()] = State.VISITED;
	}
}