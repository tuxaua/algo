/**
 * Example for dynamic programming. See CLRS 3rd edition (2009).
 */ 
public abstract class Solver {
    private final int[] p;
    
    protected Solver(int[] price) {
        p = price.clone();
    }
    
    protected int price(int n) {
        return p[n];
    }
    
    public abstract int cutRod(int n);
}
