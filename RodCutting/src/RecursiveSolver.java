public class RecursiveSolver extends Solver {

    public RecursiveSolver(int[] price) {
        super(price);
    }
  
    @Override
    public int cutRod(int n) {
        int q = 0;
        for (int i = 1; i <= n; i++) {
            q = Math.max(q, price(i) + cutRod(n - i));
        }
        return q;
    }
}
