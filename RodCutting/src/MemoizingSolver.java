public class MemoizingSolver extends Solver {

    public MemoizingSolver(int[] price) {
        super(price);
    }

    @Override
    public int cutRod(int n) {
        int[] r = new int[n + 1];
        return cutRodAux(n, r);
    }

    private int cutRodAux(int n, int[] r) {
        int q = 0;
        if (n > 0 && r[n] == 0) {
            for (int i = 1; i <= n; i++) {
                q = Math.max(q, price(i) + cutRodAux(n - i, r));
            }
            r[n] = q;
        }
        return q;
    }
}
