package kds.algorithms.sort;

import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class InsertionSortTest {

	private Integer[] array = new Integer[100000];
	
	@Before
	public void setUp() {
		Random random = new Random();
		for (int i = 0; i < array.length; i++) {
			array[i] = random.nextInt(10000);
		}
	}
	
	@Test
	public void testSort() {
		InsertionSort.sort(array);
		for (int i = 1; i < array.length; i++) {
			assertTrue(array[i - 1] <= array[i]);
		}
	}
}
