package kds.algorithms.lcm;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LongestCommonSubsequenceTest {

	@Test
	public void testLength() {
		assertEquals(4, LongestCommonSubsequence.length("ABCDEFG", "BCDGK"));
		assertEquals(4, LongestCommonSubsequence.length("BANANA", "ATANA"));
	}

}
