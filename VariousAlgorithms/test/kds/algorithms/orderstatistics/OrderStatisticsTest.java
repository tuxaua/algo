package kds.algorithms.orderstatistics;

import java.util.Random;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class OrderStatisticsTest {

	private static final int N = 100000;

	private Random rand = new Random();
	private int[] elements = new int[N];

	@Before
	public void setUp() {
		for (int i = 0; i < N; i++) {
			elements[i] = i;
		}
		for (int n = 0; n < 7 * N; n++) {
			int r1 = rand.nextInt(N);
			int r2 = rand.nextInt(N);
			int tmp = elements[r1];
			elements[r1] = elements[r2];
			elements[r2] = tmp;
		}
	}

	@Test
	public void testRandSelect() {
		for (int i = 0; i < 5000; i++) {
			int r = rand.nextInt(N) + 1;
			Assert.assertEquals("i=" + i, r - 1,
					OrderStatistics.randSelect(elements, r));
		}
	}
}
