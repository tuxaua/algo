package kds.algorithms.heap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class BinaryMaxHeapTest {
	private static final int ELEMENTS = 1000000;

	private BinaryMaxHeap<Integer> heap;

	@Before
	public void createElements() {
		heap = new BinaryMaxHeap<Integer>();
		Random rand = new Random();
		for (int i = 0; i < ELEMENTS; i++) {
			Integer elem = rand.nextInt();
			heap.insert(elem);
		}
	}

	@Test
	public void testInsert() {
		assertEquals("heap size", ELEMENTS, heap.size());
	}

	@Test
	public void testExtractMax() {
		Integer maximum = heap.extractMax();
		Integer previous = maximum;
		while (heap.size() > 0) {
			Integer current = heap.extractMax();
			assertTrue(previous.compareTo(current) >= 0);
			assertTrue(maximum.compareTo(current) >= 0);
			previous = current;
		}
	}
}
