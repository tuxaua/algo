package kds.algorithms.fibonacci;

import static org.junit.Assert.*;

import org.junit.Test;

public class FibonacciTest {

	@Test
	public void testFib() {
		assertEquals("fib(0) == 0", 0, Fibonacci.fib(0));
		assertEquals("fib(1) == 1", 1, Fibonacci.fib(1));
		assertEquals("fib(2) == 1", 1, Fibonacci.fib(2));
		assertEquals("fib(3) == 2", 2, Fibonacci.fib(3));
		assertEquals("fib(4) == 3", 3, Fibonacci.fib(4));
		assertEquals("fib(5) == 5", 5, Fibonacci.fib(5));
		assertEquals("fib(6) == 8", 8, Fibonacci.fib(6));
		assertEquals("fib(7) == 13", 13, Fibonacci.fib(7));
		assertEquals("fib(8) == 21", 21, Fibonacci.fib(8));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFibIllegalArgumentException() {
		Fibonacci.fib(-1);
	}
	
	@Test
	public void testFibDynamicProgramming() {
		assertEquals("fib(0) == 0", 0, Fibonacci.fibDynamicProgramming(0));
		assertEquals("fib(1) == 1", 1, Fibonacci.fibDynamicProgramming(1));
		assertEquals("fib(2) == 1", 1, Fibonacci.fibDynamicProgramming(2));
		assertEquals("fib(3) == 2", 2, Fibonacci.fibDynamicProgramming(3));
		assertEquals("fib(4) == 3", 3, Fibonacci.fibDynamicProgramming(4));
		assertEquals("fib(5) == 5", 5, Fibonacci.fibDynamicProgramming(5));
		assertEquals("fib(6) == 8", 8, Fibonacci.fibDynamicProgramming(6));
		assertEquals("fib(7) == 13", 13, Fibonacci.fibDynamicProgramming(7));
		assertEquals("fib(8) == 21", 21, Fibonacci.fibDynamicProgramming(8));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testFibDynamicProgrammingIllegalArgumentException() {
		Fibonacci.fibDynamicProgramming(-1);
	}
}
