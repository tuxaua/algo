package kds.algorithms.skiplist;

public class SkipList {
	
	private static class Node {
		final Node[] mNext;
		final int mValue;
		
		public Node(int value, int level) {
			mValue = value;
			mNext = new Node[level + 1];
		}	
	}
	
	private static final int MAX_LEVEL = 31;	
	
	private RandomBooleanGenerator mGenerator = new RandomBooleanGenerator();
	
	private final Node mHead = new Node(0, MAX_LEVEL);

	private int mHighestLevel = 0;

	public void add(int x) {
		
		final int level = calculateLevel();
		final Node node = new Node(x, level);
		
		Node current = mHead, next;
		for (int i = mHighestLevel; i >= 0; i--) {			
			while ((next = current.mNext[i]) != null && next.mValue < x) {
				current = next;
			}
			if (i <= level) {
				node.mNext[i] = current.mNext[i];
				current.mNext[i] = node;
			}
		}
	}
	
	private int calculateLevel() {
		int level = 0;
		final int upper = Math.max(MAX_LEVEL, mHighestLevel + 1);
		
		while (level < upper && mGenerator.next()) {
			level++;
		}
		if (level > mHighestLevel) {
			mHighestLevel = level;
		}
		return level;
	}

	public boolean contains(int x) {
		Node current = mHead, next;
		for (int i = mHighestLevel; i >= 0; i--) {
			while ((next = current.mNext[i]) != null) {
				if (next.mValue < x) {
					current = next;
				} else if (next.mValue == x) {
					return true;
				} else {
					break;
				}
			}
		}
		return false;
	}
	
	public boolean remove(int x) {
		boolean res = false;
		Node current = mHead;
		for (int i = mHighestLevel; i >= 0; i--) {
			Node next;
			
			while ((next = current.mNext[i]) != null) {
				if (next.mValue < x) {
					current = next;
				} else if (next.mValue == x) {
					res = true;
					current.mNext[i] = next.mNext[i];
					break;
				} else {
					break;
				}
			}
		}
		return res;
	}

	int[] getSize() {
		final int[] res = new int[mHighestLevel + 1];

		for (int i = 0; i < res.length; i++) {
			for (Node next = mHead.mNext[i]; next != null; next = next.mNext[i]) {
				res[i]++;
			}
		}
		
		return res;
	}
}
