package kds.algorithms.philosophers;

class Forks {
	private boolean[] forkInUse;
	
	public Forks(int n) {
		forkInUse = new boolean[n];
	}
	
	public boolean isAvailable(int k) {
		return !forkInUse[k];
	}
	
	public void pickup(int k) {
		if (forkInUse[k]) {
			throw new IllegalStateException("Fork " + k + "is being used");
		}
		forkInUse[k] = true;
	}
	
	public void putdown(int k) {
		if (!forkInUse[k]) {
			throw new IllegalStateException("Fork " + k + "is not in use");
		}
		forkInUse[k] = false;
	}
}
