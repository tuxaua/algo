package kds.algorithms.philosophers;

import java.util.logging.Logger;

public class Philosopher implements Runnable {
	private static final Logger logger = Logger.getLogger("Philosopher");
	
	private final int id;
	private final Forks forks;
	private final int left;
	private final int right;
	
	public Philosopher(int id, Forks forks, int left, int right) {
		this.id = id;
		this.forks = forks;
		this.left = left;
		this.right = right;
	}

	private void pickup() {
		logger.info("Philosopher " + id + " wants to take forks");
		synchronized (forks) {
			while (!forks.isAvailable(left) || !forks.isAvailable(right)) {
				try {
					forks.wait();
				} catch (InterruptedException e) {
					logger.info("Philosopher " + id + " was interrupted while waiting for forks");
				}
			}
			forks.pickup(left);
			forks.pickup(right);
		}
		logger.info("Philosopher " + id + " took the forks");
	}
	
	private void putdown() {
		synchronized (forks) {
			forks.putdown(left);
			forks.putdown(right);
			forks.notifyAll();
		}
	}
	
	private void eat() {
		logger.info("Philosopher " + id + " starts to eat");
		try {
			Thread.sleep((long) (Math.random() * 10000));
		} catch (InterruptedException e) {
			logger.info("Philosopher " + id + " was interrupted while eating");
		}
		logger.info("Philosopher " + id + " finished eating");
	}

	private void think() {
		logger.info("Philosopher " + id + " starts to think");
		try {
			Thread.sleep((long) (Math.random() * 20000));
		} catch (InterruptedException e) {
			System.out.println("Philosopher " + id + " was interrupted while thinking");
		}
		logger.info("Philosopher " + id + " finished thinking");
	}

	@Override
	public void run() {
		while (true) {
			think();
			pickup();
			eat();
			putdown();
		}
	}
	
	public static void main(String[] args) {
		final int n = 3;
		Forks forks = new Forks(n);
		Philosopher[] p = new Philosopher[n];
		for (int i = 0; i < p.length; i++) {
			p[i] = new Philosopher(i, forks, i, (i + 1) % n);
			new Thread(p[i]).start();
		}
	}
}
