package kds.algorithms.heap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BinaryMaxHeap<E extends Comparable<? super E>> {

	private List<E> a = new ArrayList<E>();
	
	public void changeKey(E e1, E e2) {
		// TODO missing implementation
	}
	
	public E extractMax() {
		final E res = a.get(0);
		E lastElement = a.remove(a.size() - 1);
		if (!a.isEmpty()) {
			a.set(0, lastElement);
			moveDown(0);
		}
		return res;
	}
	
	public void insert(E e) {
		a.add(e);
		moveUp(a.size() - 1);
	}
	
	private void moveDown(int pos) {
		final int last = a.size() - 1;
		int current = pos, child;
	
		while ((child = 2 * current + 1) <= last) {
			if (child < last) {
				// current node has two children, select the greater of them
				if (a.get(child).compareTo(a.get(child + 1)) < 0) {
					child++;
				}
			}
			if (a.get(current).compareTo(a.get(child)) > 0) {
				// current node is greater than children, nothing left to do
				return;
			}
			Collections.swap(a, current, child);
			current = child;
		}
	}
	
	private void moveUp(int pos) {
		int current = pos;
		while (current > 0) {
			final int parent = (current - 1) / 2;
			if (a.get(current).compareTo(a.get(parent)) <= 0) {
				return;
			}
			Collections.swap(a, current, parent);
			current = parent;
		}
	}
	
	public int size() {
		return a.size();
	}
}
