package kds.algorithms.orderstatistics;

import java.util.Random;

/**
 * Calculates the i-th smallest element of an array in expected linear time.
 * The array can be in any order but it must not contain any duplicate values.
 * 
 */
public class OrderStatistics {

	private static final Random rand = new Random();
	
	public static int randSelect(int[] a, int i) {
		return randSelect(a.clone(), 0, a.length - 1, i);
	}

	private static int randSelect(int[] a, int p, int q, int i) {
		while (p < q) {
			int r = randPartition(a, p, q);
			int k = r - p + 1;
			if (i == k) {
				return a[r];
			} else if (i < k) {
				q = r - 1;			// randSelect(a, p, r - 1, i)
			} else {
				p = r + 1;			// randSelect(a, r + 1, q, i - k)
				i -= k;
			}
		}
		return a[p];
	}
	
//	private static int randSelect(int[] a, int p, int q, int i) {
//		if (p == q) {
//			return a[p];
//		}
//		int r = randPartition(a, p, q);
//		int k = r - p + 1;
//		if (i == k) {
//			return a[r];
//		} else if (i < k) {
//			return randSelect(a, p, r - 1, i);
//		} else {
//			return randSelect(a, r + 1, q, i - k);
//		}
//	}
	
	private static int randPartition(int[] a, int p, int q) {
		final int r = rand.nextInt(q - p + 1) + p;
		final int pivot = a[r];
		a[r] = a[p];
		a[p] = pivot;
		int i = p;
		for (int j = p + 1; j <= q; j++) {
			if (a[j] <= pivot) {
				final int tmp = a[++i];
				a[i] = a[j];
				a[j] = tmp;
			}
		}
		a[p] = a[i];
		a[i] = pivot;
		return i;
	}
}
