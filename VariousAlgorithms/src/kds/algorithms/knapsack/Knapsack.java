package kds.algorithms.knapsack;

public class Knapsack {

	public static void main(String[] args) {
		final int[] v = { 1, 3, 5, 7, 11, 17, 23, 30 };
		final int[] w = { 2, 4, 4, 9, 10, 15, 19, 28 };
		int res = solve(v, w, 50);
		System.out.println(res);
	}

	public static int solve(int[] v, int[] w, int wmax) {
		final int n = v.length;
		int[][] t = new int[n + 1][wmax + 1];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j <= wmax; j++) {
				if (w[i] <= j) {
					t[i + 1][j] = Math.max(t[i][j], v[i] + t[i][j - w[i]]);
				} else {
					t[i + 1][j] = t[i][j];
				}
			}
		}
		return t[n][wmax];
	}
}
