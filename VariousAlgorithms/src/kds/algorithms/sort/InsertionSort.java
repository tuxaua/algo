package kds.algorithms.sort;

public class InsertionSort {

	public static <E extends Comparable<? super E>> void sort(E[] a) {
		for (int i = 1; i < a.length; i++) {
			E key = a[i];
			int j = i;
			while (j > 0 && a[j - 1].compareTo(key) >= 0) {
				a[j] = a[j - 1];
				j--;
			}
			a[j] = key;
		}
	}
}