package kds.algorithms.graph;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

public class TopologicalSortTest {

	@Test
	public void testSort() {
		Graph g = new Graph(8);
		Vertex[] vertices = g.vertices();
		g.createEdge(vertices[1], vertices[4]);
		g.createEdge(vertices[1], vertices[6]);
		g.createEdge(vertices[2], vertices[7]);
		g.createEdge(vertices[3], vertices[4]);
		g.createEdge(vertices[3], vertices[7]);
		g.createEdge(vertices[4], vertices[5]);
		g.createEdge(vertices[7], vertices[0]);
		g.createEdge(vertices[7], vertices[5]);
		g.createEdge(vertices[7], vertices[6]);
		List<Vertex> res = TopologicalSort.sort(g);
		assertEquals(vertices.length, res.size());
		for (Vertex v : res) {
			System.out.print(v.id + " ");
		}
		System.out.println();
	}
}
