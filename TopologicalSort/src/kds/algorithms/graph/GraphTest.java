package kds.algorithms.graph;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class GraphTest {

	private Graph g;
	private Vertex[] vertices;
	
	@Before
	public void setUp() {
		g = new Graph(8);
		vertices = g.vertices();
		g.createEdge(vertices[1], vertices[4]);
		g.createEdge(vertices[1], vertices[6]);
		g.createEdge(vertices[2], vertices[7]);
		g.createEdge(vertices[3], vertices[4]);
		g.createEdge(vertices[3], vertices[7]);
		g.createEdge(vertices[4], vertices[5]);
		g.createEdge(vertices[7], vertices[0]);
		g.createEdge(vertices[7], vertices[5]);
		g.createEdge(vertices[7], vertices[6]);
	}

	@Test
	public void testNeighbors() {
		Set<Vertex> neighbors = g.neighbors(vertices[3]);
		assertEquals(2, neighbors.size());
		assertTrue(neighbors.contains(vertices[4]));
		assertTrue(neighbors.contains(vertices[7]));
	}

	@Test
	public void testNumEdgesIn() {
		assertEquals(1, g.numEdgesIn(vertices[0]));
		assertEquals(0, g.numEdgesIn(vertices[1]));
		assertEquals(0, g.numEdgesIn(vertices[2]));
		assertEquals(0, g.numEdgesIn(vertices[3]));
		assertEquals(2, g.numEdgesIn(vertices[4]));
		assertEquals(2, g.numEdgesIn(vertices[5]));
		assertEquals(2, g.numEdgesIn(vertices[6]));
		assertEquals(2, g.numEdgesIn(vertices[7]));
	}

	@Test
	public void testNumEdges() {
		assertEquals(9, g.numEdges());
	}
}
