package kds.algorithms.graph;

import java.util.LinkedList;
import java.util.List;

public class TopologicalSort {
	public static List<Vertex> sort(Graph g) {
		List<Vertex> res = new LinkedList<Vertex>();
		List<Vertex> s = new LinkedList<Vertex>();
		for (Vertex v : g.vertices()) {
			if (g.numEdgesIn(v) == 0) {
				s.add(v);
			}
		}
		while (!s.isEmpty()) {
			Vertex v = s.remove(0);
			res.add(v);
			for (Vertex w : g.neighbors(v)) {
				g.removeEdge(v, w);
				if (g.numEdgesIn(w) == 0) {
					s.add(w);
				}
			}
		}
		if (g.numEdges() > 0) {
			throw new IllegalArgumentException("graph contains cycle");
		}
		return res;
	}
}
