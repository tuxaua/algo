package kds.algorithms.graph;

class Vertex {
	final int id;
	
	private Vertex(int id) {
		this.id = id;
	}
	
	@Override
	public boolean equals(Object o) {
		return o instanceof Vertex && ((Vertex) o).id == id;
	}
	
	@Override
	public int hashCode() {
		return id;
	}
	
	@Override
	public String toString() {
		return "Vertex[id=" + id +"]";
	}

	public static Vertex getInstance(int id) {
		return new Vertex(id);
	}
}