package kds.algorithms.graph;

class Vertex {
	public final String id;

	public Vertex(String id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}

	@Override
	public String toString() {
		return "Vertex[" + id + "]";
	}
}
