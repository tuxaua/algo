package kds.algorithms.graph;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.After;
import org.junit.Test;

public class FlowNetworkTest {

	private FlowNetwork g = new FlowNetwork();
	private Map<String, Vertex> vertices = new HashMap<String, Vertex>();

	private void addVertices(String[] ids) {
		for (String s : ids) {
			vertices.put(s, g.addVertex(s));
		}
		
	}
	
	@After
	public void printEdges() {
		System.out.println(g);
	}

	@Test
	public void testCalculateMaxFlow1() {
		addVertices(new String[] { "s", "o", "p", "q", "r", "t" });

		g.addEdge(vertices.get("s"), vertices.get("o"), 3);
		g.addEdge(vertices.get("s"), vertices.get("p"), 3);
		g.addEdge(vertices.get("o"), vertices.get("p"), 2);
		g.addEdge(vertices.get("o"), vertices.get("q"), 3);
		g.addEdge(vertices.get("p"), vertices.get("r"), 2);
		g.addEdge(vertices.get("r"), vertices.get("t"), 3);
		g.addEdge(vertices.get("q"), vertices.get("r"), 4);
		g.addEdge(vertices.get("q"), vertices.get("t"), 2);

		assertEquals(5,
				g.calculateMaxFlow(vertices.get("s"), vertices.get("t")));
	}
	
	@Test
	public void testCalculateMaxFlow2() {
		addVertices(new String[] { "q", "u", "v", "s" });

		g.addEdge(vertices.get("q"), vertices.get("u"), 4);
		g.addEdge(vertices.get("q"), vertices.get("v"), 2);
		g.addEdge(vertices.get("u"), vertices.get("v"), 3);
		g.addEdge(vertices.get("u"), vertices.get("s"), 1);
		g.addEdge(vertices.get("v"), vertices.get("s"), 6);

		assertEquals(6,
				g.calculateMaxFlow(vertices.get("q"), vertices.get("s")));
	}

}
